-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2020 at 02:05 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `khachsan`
--

-- --------------------------------------------------------

--
-- Table structure for table `chucvu`
--

CREATE TABLE `chucvu` (
  `ma_chuc_vu` int(11) NOT NULL,
  `ten_chuc_vu` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chucvu`
--

INSERT INTO `chucvu` (`ma_chuc_vu`, `ten_chuc_vu`) VALUES
(1, 'Giám đốc'),
(2, 'Nhân viên');

-- --------------------------------------------------------

--
-- Table structure for table `datphong`
--

CREATE TABLE `datphong` (
  `ma_dat_phong` int(11) NOT NULL,
  `ho_ten` varchar(100) DEFAULT NULL,
  `sodt` varchar(15) DEFAULT NULL,
  `socmnd` varchar(20) DEFAULT NULL,
  `tien_coc` decimal(12,0) DEFAULT NULL,
  `ma_phong` int(11) DEFAULT NULL,
  `ngay_dat` date DEFAULT NULL,
  `gio_dat` time DEFAULT NULL,
  `loai_dat` varchar(255) DEFAULT NULL,
  `ten_dang_nhap` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `datphong`
--

INSERT INTO `datphong` (`ma_dat_phong`, `ho_ten`, `sodt`, `socmnd`, `tien_coc`, `ma_phong`, `ngay_dat`, `gio_dat`, `loai_dat`, `ten_dang_nhap`) VALUES
(1, 'Nguyễn Quốc Cảnh', '0911776579', '233213653', '100000', 1, '2017-10-21', '10:00:00', 'theongay', 'admin'),
(2, 'Trần Thị Thùy Dung', '01664550730', '243596874', '50000', 2, '2017-10-21', '10:00:00', 'theogio', 'admin'),
(3, 'Chế Bồng Gia', '01658551236', '253412257', '50000', 3, '2017-10-21', '10:00:00', 'theogio', 'admin'),
(4, 'Trịnh Ngọc Cả', '01658554223', '263541254', '50000', 7, '2017-10-21', '10:00:00', 'theogio', 'admin'),
(5, 'Lê Kinh Sáng', '01658325423', '283596874', '50000', 8, '2017-10-21', '10:00:00', 'theongay', 'admin'),
(6, 'Nguyễn Văn Phúc', '01678551236', '233854257', '50000', 9, '2017-10-22', '10:00:00', 'theogio', 'admin'),
(7, 'Lê Thị Hải', '01652644223', '233541457', '50000', 13, '2017-10-25', '10:00:00', 'theogio', 'admin'),
(8, 'Nguyễn Quốc Dũng', '01658325423', '233522254', '50000', 14, '2017-10-27', '10:00:00', 'theongay', 'admin'),
(9, 'Trần Văn Dũng', '01658741214', '233412257', '50000', 15, '2017-10-27', '10:00:00', 'theongay', 'admin'),
(10, 'Nguyễn Văn Khôi', '01659658525', '233547457', '50000', 19, '2017-10-29', '10:00:00', 'theongay', 'admin'),
(11, 'Nguyễn Đăng Khoa', '01658322414', '233596874', '50000', 20, '2017-10-29', '10:00:00', 'theongay', 'admin'),
(12, 'Lê Đình Hiếu', '01658558942', '233412586', '50000', 21, '2017-10-29', '10:00:00', 'theongay', 'admin'),
(13, 'Minh Vương M4U', '016585525', '262341224', '50000', 25, '2017-10-29', '10:00:00', 'theongay', 'admin'),
(14, 'Sơn Tùng MTV', '01658321453', '287496874', '50000', 26, '2017-10-29', '10:00:00', 'theongay', 'admin'),
(15, 'Mr.Siro', '01658625336', '233572257', '50000', 27, '2017-10-29', '10:00:00', 'theongay', 'admin'),
(16, 'hpvodanh', '1233333', '12313', '300000', 4, '2020-12-28', '20:02:00', 'theongay', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `dichvu`
--

CREATE TABLE `dichvu` (
  `ma_dich_vu` int(11) NOT NULL,
  `ten_dich_vu` varchar(50) NOT NULL,
  `loai_dich_vu` int(11) NOT NULL,
  `gia_dich_vu` decimal(12,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dichvu`
--

INSERT INTO `dichvu` (`ma_dich_vu`, `ten_dich_vu`, `loai_dich_vu`, `gia_dich_vu`) VALUES
(1, 'Pepsi', 0, '10000'),
(2, 'Coca', 0, '10000'),
(3, 'Rượu CHIVAS', 0, '550000'),
(4, 'Rượu vang', 0, '300000'),
(5, 'Bò hút', 0, '20000'),
(6, 'Thuốc con ngựa', 0, '30000'),
(7, 'Thuốc con ó', 0, '20000'),
(8, 'Hamburger', 0, '30000'),
(9, 'Cơm tấm', 0, '30000'),
(10, 'Cơm gà', 0, '30000'),
(11, 'Cơm thập cẩm', 0, '30000'),
(12, 'Nước lọc', 0, '5000'),
(13, 'Gà KFC', 0, '100000'),
(14, 'Bò kho', 0, '30000'),
(15, 'Bò tái', 0, '30000'),
(16, 'Mì trứng', 0, '15000'),
(17, 'Giặt ủi', 1, '10000'),
(18, 'Vé massage', 2, '50000'),
(19, 'Vé bể bơi', 2, '30000'),
(20, 'Vé phòng tập gym', 2, '30000');

-- --------------------------------------------------------

--
-- Table structure for table `dondichvu`
--

CREATE TABLE `dondichvu` (
  `ma_don_dich_vu` int(11) NOT NULL,
  `ma_dat_phong` int(11) NOT NULL,
  `ma_dich_vu` int(11) NOT NULL,
  `so_luong` int(11) NOT NULL,
  `ngay_dat` date DEFAULT NULL,
  `gio_dat` time DEFAULT NULL,
  `thong_tin_them` varchar(255) DEFAULT NULL,
  `ten_dang_nhap` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dondichvu`
--

INSERT INTO `dondichvu` (`ma_don_dich_vu`, `ma_dat_phong`, `ma_dich_vu`, `so_luong`, `ngay_dat`, `gio_dat`, `thong_tin_them`, `ten_dang_nhap`) VALUES
(1, 1, 18, 4, '2017-10-21', '10:00:00', NULL, 'admin'),
(2, 1, 8, 2, '2017-10-21', '10:00:00', NULL, 'admin'),
(3, 1, 4, 2, '2017-10-21', '10:00:00', NULL, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `giaodien`
--

CREATE TABLE `giaodien` (
  `ma_giao_dien` int(2) NOT NULL,
  `ten_to_chuc` varchar(25) DEFAULT NULL,
  `dia_chi` varchar(60) DEFAULT NULL,
  `so_dien_thoai` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `giaodien`
--

INSERT INTO `giaodien` (`ma_giao_dien`, `ten_to_chuc`, `dia_chi`, `so_dien_thoai`) VALUES
(1, 'Khách sạn Homie', '23 Lê Thanh Nghị Quận hải châu thành phố đà nẵng', '0911111111');

-- --------------------------------------------------------

--
-- Table structure for table `lichdatphong`
--

CREATE TABLE `lichdatphong` (
  `ma_lich` int(11) NOT NULL,
  `ten_nguoi_dat` varchar(50) NOT NULL,
  `so_dien_thoai` varchar(20) NOT NULL,
  `ngay_dat` date NOT NULL,
  `gio_dat` time NOT NULL,
  `thong_tin_them` varchar(255) DEFAULT NULL,
  `ten_dang_nhap` varchar(60) NOT NULL,
  `ma_phong` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lichsudangnhap`
--

CREATE TABLE `lichsudangnhap` (
  `ma_lich_su_dang_nhap` int(11) NOT NULL,
  `ngay_dang_nhap` date NOT NULL,
  `gio_dang_nhap` time NOT NULL,
  `tai_khoan_dang_nhap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lichsudangnhap`
--

INSERT INTO `lichsudangnhap` (`ma_lich_su_dang_nhap`, `ngay_dang_nhap`, `gio_dang_nhap`, `tai_khoan_dang_nhap`) VALUES
(1, '2020-12-25', '21:07:42', 'admin'),
(2, '2020-12-25', '22:51:44', 'admin'),
(3, '2020-12-25', '22:54:53', 'admin'),
(4, '2020-12-25', '23:03:23', 'admin'),
(5, '2020-12-25', '23:18:36', 'admin'),
(6, '2020-12-25', '23:23:52', 'admin'),
(7, '2020-12-25', '23:33:12', 'admin'),
(8, '2020-12-25', '23:37:58', 'admin'),
(9, '2020-12-25', '23:38:25', 'admin'),
(10, '2020-12-25', '23:38:32', 'admin'),
(11, '2020-12-25', '23:45:57', 'admin'),
(12, '2020-12-25', '23:46:04', 'admin'),
(13, '2020-12-25', '23:51:41', 'admin'),
(14, '2020-12-25', '23:55:41', 'admin'),
(15, '2020-12-25', '23:56:56', 'admin'),
(16, '2020-12-26', '15:34:34', 'admin'),
(17, '2020-12-26', '16:58:49', 'admin'),
(18, '2020-12-26', '17:02:54', 'admin'),
(19, '2020-12-26', '17:15:32', 'admin'),
(20, '2020-12-26', '17:19:25', 'admin'),
(21, '2020-12-26', '17:20:38', 'admin'),
(22, '2020-12-26', '18:13:59', 'admin'),
(23, '2020-12-26', '18:27:28', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `loaiphong`
--

CREATE TABLE `loaiphong` (
  `ma_loai_phong` int(11) NOT NULL,
  `ten_loai_phong` varchar(100) NOT NULL,
  `mo_ta` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loaiphong`
--

INSERT INTO `loaiphong` (`ma_loai_phong`, `ten_loai_phong`, `mo_ta`) VALUES
(1, 'VIP', 'Phòng rộng thoáng mát, tiện nghi đầy đủ.'),
(2, 'Thường', 'Phòng thường giá rẻ.'),
(3, 'Homestay', 'Phòng dành cho hộ gia đình, đi từ 4 người trở lên.');

-- --------------------------------------------------------

--
-- Table structure for table `phong`
--

CREATE TABLE `phong` (
  `ma_phong` int(11) NOT NULL,
  `so_phong` int(11) NOT NULL,
  `tang` int(2) DEFAULT NULL,
  `tien_nghi` varchar(250) DEFAULT NULL,
  `ma_loai_phong` int(11) NOT NULL,
  `hinh_anh` varchar(255) DEFAULT NULL,
  `gia_phong` decimal(12,0) NOT NULL,
  `gia_phong_gio_dau` decimal(12,0) NOT NULL,
  `gia_phong_gio_sau` decimal(12,0) NOT NULL,
  `gia_homestay` decimal(12,0) NOT NULL,
  `khuyen_mai` varchar(250) DEFAULT NULL,
  `trang_thai` int(1) NOT NULL,
  `count_homestay` int(11) DEFAULT NULL,
  `count_dat_lich` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phong`
--

INSERT INTO `phong` (`ma_phong`, `so_phong`, `tang`, `tien_nghi`, `ma_loai_phong`, `hinh_anh`, `gia_phong`, `gia_phong_gio_dau`, `gia_phong_gio_sau`, `gia_homestay`, `khuyen_mai`, `trang_thai`, `count_homestay`, `count_dat_lich`) VALUES
(1, 101, 1, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '1.jpg', '300000', '50000', '20000', '80000', '20', 1, NULL, NULL),
(2, 102, 1, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '2.jpg ', '300000', '50000', '20000', '80000', '20', 1, NULL, NULL),
(3, 103, 1, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '3.jpg ', '300000', '50000', '20000', '80000', '20', 1, NULL, NULL),
(4, 104, 1, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '4.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(5, 105, 1, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '5.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(6, 106, 1, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '6.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(7, 201, 2, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '7.jpg ', '300000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(8, 202, 2, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '8.jpg ', '300000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(9, 203, 2, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '9.jpg ', '300000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(10, 204, 2, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '10.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(11, 205, 2, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '11.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(12, 206, 2, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '12.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(13, 301, 3, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '13.jpg ', '300000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(14, 302, 3, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '14.jpg ', '300000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(15, 303, 3, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '15.jpg ', '300000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(16, 304, 3, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '16.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(17, 305, 3, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '17.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(18, 306, 3, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh', 2, '18.jpg ', '300000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(19, 401, 4, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '19.jpg ', '500000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(20, 402, 4, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '20.jpg ', '500000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(21, 403, 4, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '21.jpg ', '500000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(22, 404, 4, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '22.jpg ', '500000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(23, 405, 4, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '23.jpg ', '500000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(24, 406, 4, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '24.jpg ', '500000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(25, 501, 5, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '25.jpg ', '500000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(26, 502, 5, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '26.jpg ', '500000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(27, 503, 5, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '27.jpg ', '500000', '50000', '20000', '80000', '0', 1, NULL, NULL),
(28, 504, 5, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '28.jpg ', '500000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(29, 505, 5, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '29.jpg ', '500000', '50000', '20000', '80000', '0', 0, NULL, NULL),
(30, 506, 5, 'Điều hòa,Quạt,Tivi,Nước nóng lạnh,Vị trí tốt', 1, '30.jpg ', '500000', '50000', '20000', '80000', '0', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taikhoan`
--

CREATE TABLE `taikhoan` (
  `ten_dang_nhap` varchar(50) NOT NULL,
  `mat_khau` varchar(255) NOT NULL,
  `ho_ten` varchar(100) NOT NULL,
  `gioi_tinh` varchar(5) NOT NULL,
  `ngay_sinh` date NOT NULL,
  `cmnd` varchar(20) NOT NULL,
  `sodt` varchar(15) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `ngay_tao` date DEFAULT NULL,
  `gio_tao` time DEFAULT NULL,
  `ma_chuc_vu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taikhoan`
--

INSERT INTO `taikhoan` (`ten_dang_nhap`, `mat_khau`, `ho_ten`, `gioi_tinh`, `ngay_sinh`, `cmnd`, `sodt`, `email`, `ngay_tao`, `gio_tao`, `ma_chuc_vu`) VALUES
('admin', 'MTIzNDU2Nzg=', 'Admin', 'Nam', '1995-09-18', '233213653', '01664550790', 'pigtrumkt@gmail.com', '2019-01-01', '10:00:00', 1),
('guest', 'MTIzNDU2Nzg=', 'Guest', 'Nam', '1995-08-20', '236523152', '0911547526', ' pigtrumkt@gmail.com', '2019-01-01', '10:00:00', 2);

-- --------------------------------------------------------

--
-- Table structure for table `thongsotheogio`
--

CREATE TABLE `thongsotheogio` (
  `ma_thong_so` int(11) NOT NULL,
  `bao_nhieu_gio_dau` int(11) NOT NULL,
  `so_gio_chuyen_thanh_ngay` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `thongsotheogio`
--

INSERT INTO `thongsotheogio` (`ma_thong_so`, `bao_nhieu_gio_dau`, `so_gio_chuyen_thanh_ngay`) VALUES
(1, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `thuchi`
--

CREATE TABLE `thuchi` (
  `ma_thu_chi` int(11) NOT NULL,
  `noi_dung_chi` varchar(255) NOT NULL,
  `loai_thu_chi` int(1) NOT NULL,
  `ngay_chi` date DEFAULT NULL,
  `gio_chi` time DEFAULT NULL,
  `so_tien` decimal(12,0) NOT NULL,
  `ten_dang_nhap` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `traphong`
--

CREATE TABLE `traphong` (
  `ma_tra_phong` int(11) NOT NULL,
  `ma_dat_phong` int(11) NOT NULL,
  `ngay_tra` date NOT NULL,
  `gio_tra` time NOT NULL,
  `tong_tien` decimal(12,0) NOT NULL,
  `nguoi_thu_tien` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `traphong`
--

INSERT INTO `traphong` (`ma_tra_phong`, `ma_dat_phong`, `ngay_tra`, `gio_tra`, `tong_tien`, `nguoi_thu_tien`) VALUES
(1, 16, '2020-12-26', '20:03:00', '0', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chucvu`
--
ALTER TABLE `chucvu`
  ADD PRIMARY KEY (`ma_chuc_vu`);

--
-- Indexes for table `datphong`
--
ALTER TABLE `datphong`
  ADD PRIMARY KEY (`ma_dat_phong`),
  ADD KEY `ma_phong` (`ma_phong`);

--
-- Indexes for table `dichvu`
--
ALTER TABLE `dichvu`
  ADD PRIMARY KEY (`ma_dich_vu`);

--
-- Indexes for table `dondichvu`
--
ALTER TABLE `dondichvu`
  ADD PRIMARY KEY (`ma_don_dich_vu`),
  ADD KEY `ma_dat_phong` (`ma_dat_phong`),
  ADD KEY `ma_dich_vu` (`ma_dich_vu`);

--
-- Indexes for table `giaodien`
--
ALTER TABLE `giaodien`
  ADD PRIMARY KEY (`ma_giao_dien`);

--
-- Indexes for table `lichdatphong`
--
ALTER TABLE `lichdatphong`
  ADD PRIMARY KEY (`ma_lich`),
  ADD KEY `ma_phong` (`ma_phong`);

--
-- Indexes for table `lichsudangnhap`
--
ALTER TABLE `lichsudangnhap`
  ADD PRIMARY KEY (`ma_lich_su_dang_nhap`);

--
-- Indexes for table `loaiphong`
--
ALTER TABLE `loaiphong`
  ADD PRIMARY KEY (`ma_loai_phong`);

--
-- Indexes for table `phong`
--
ALTER TABLE `phong`
  ADD PRIMARY KEY (`ma_phong`),
  ADD KEY `ma_loai_phong` (`ma_loai_phong`);

--
-- Indexes for table `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD PRIMARY KEY (`ten_dang_nhap`),
  ADD KEY `ma_chuc_vu` (`ma_chuc_vu`);

--
-- Indexes for table `thongsotheogio`
--
ALTER TABLE `thongsotheogio`
  ADD PRIMARY KEY (`ma_thong_so`);

--
-- Indexes for table `thuchi`
--
ALTER TABLE `thuchi`
  ADD PRIMARY KEY (`ma_thu_chi`);

--
-- Indexes for table `traphong`
--
ALTER TABLE `traphong`
  ADD PRIMARY KEY (`ma_tra_phong`),
  ADD KEY `ma_dat_phong` (`ma_dat_phong`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chucvu`
--
ALTER TABLE `chucvu`
  MODIFY `ma_chuc_vu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `datphong`
--
ALTER TABLE `datphong`
  MODIFY `ma_dat_phong` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `dichvu`
--
ALTER TABLE `dichvu`
  MODIFY `ma_dich_vu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `dondichvu`
--
ALTER TABLE `dondichvu`
  MODIFY `ma_don_dich_vu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `giaodien`
--
ALTER TABLE `giaodien`
  MODIFY `ma_giao_dien` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `lichdatphong`
--
ALTER TABLE `lichdatphong`
  MODIFY `ma_lich` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lichsudangnhap`
--
ALTER TABLE `lichsudangnhap`
  MODIFY `ma_lich_su_dang_nhap` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `loaiphong`
--
ALTER TABLE `loaiphong`
  MODIFY `ma_loai_phong` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `phong`
--
ALTER TABLE `phong`
  MODIFY `ma_phong` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `thongsotheogio`
--
ALTER TABLE `thongsotheogio`
  MODIFY `ma_thong_so` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `thuchi`
--
ALTER TABLE `thuchi`
  MODIFY `ma_thu_chi` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `traphong`
--
ALTER TABLE `traphong`
  MODIFY `ma_tra_phong` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `datphong`
--
ALTER TABLE `datphong`
  ADD CONSTRAINT `datphong_ibfk_1` FOREIGN KEY (`ma_phong`) REFERENCES `phong` (`ma_phong`) ON DELETE CASCADE;

--
-- Constraints for table `dondichvu`
--
ALTER TABLE `dondichvu`
  ADD CONSTRAINT `dondichvu_ibfk_1` FOREIGN KEY (`ma_dat_phong`) REFERENCES `datphong` (`ma_dat_phong`) ON DELETE CASCADE,
  ADD CONSTRAINT `dondichvu_ibfk_2` FOREIGN KEY (`ma_dich_vu`) REFERENCES `dichvu` (`ma_dich_vu`) ON DELETE CASCADE;

--
-- Constraints for table `lichdatphong`
--
ALTER TABLE `lichdatphong`
  ADD CONSTRAINT `lichdatphong_ibfk_1` FOREIGN KEY (`ma_phong`) REFERENCES `phong` (`ma_phong`) ON DELETE CASCADE;

--
-- Constraints for table `phong`
--
ALTER TABLE `phong`
  ADD CONSTRAINT `phong_ibfk_1` FOREIGN KEY (`ma_loai_phong`) REFERENCES `loaiphong` (`ma_loai_phong`) ON DELETE CASCADE;

--
-- Constraints for table `taikhoan`
--
ALTER TABLE `taikhoan`
  ADD CONSTRAINT `taikhoan_ibfk_1` FOREIGN KEY (`ma_chuc_vu`) REFERENCES `chucvu` (`ma_chuc_vu`) ON DELETE CASCADE;

--
-- Constraints for table `traphong`
--
ALTER TABLE `traphong`
  ADD CONSTRAINT `traphong_ibfk_1` FOREIGN KEY (`ma_dat_phong`) REFERENCES `datphong` (`ma_dat_phong`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
